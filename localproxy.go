package main

import (
	"log"
	"net"
	"net/http"
	"os"

	"github.com/elazarl/goproxy"
)

func main() {

	// NICのIPアドレス取得
	// Windowsの場合、`ipconfig /all`で取得した対象のNICの`IPv4 Address`を環境変数に設定
	// os.Setenv("NIC_IP", "192.168.10.10")
	nic := os.Getenv("NIC_IP")

	// 対象NICのトランスポート定義
	transport := &http.Transport{
		DialContext: (&net.Dialer{
			LocalAddr: &net.TCPAddr{
				IP: net.ParseIP(nic),
			},
		}).DialContext,
	}

	// プロキシ起動
	proxy := goproxy.NewProxyHttpServer()
	proxy.Tr = transport

	proxy.Verbose = false
	proxy.KeepHeader = true
	proxy.KeepDestinationHeaders = true

	log.Fatal(http.ListenAndServe("127.0.0.1:8080", proxy))
}
