# $targetIfName = 'PANGP Virtual Ethernet Adapter';
# function Get-IPAddress {
#     param([Parameter(Mandatory, ValueFromPipelineByPropertyName)][string]$IPAddress);
#     process {
#             $IPAddress;
#     }
# }
# $env:NIC_IP = Get-NetAdapter -IncludeHidden | Where-Object InterfaceDescription -Match $targetIfName | Get-NetIPAddress -AddressFamily IPv4 | Get-IPAddress
$env:NIC_IP = Get-NetAdapter -IncludeHidden | Where-Object InterfaceDescription -Match $targetIfName | Get-NetIPAddress -AddressFamily IPv4 | Select-Object -ExpandProperty IPAddress 
Start-Process localproxy.exe -WindowStyle Hidden


# define function
# function Get-NetworkAddress {
#     param([Parameter(Mandatory, ValueFromPipelineByPropertyName)][string]$IPAddress, [Parameter(Mandatory, ValueFromPipelineByPropertyName)][int]$PrefixLength);
#     process {
#         $bitNwAddr = [ipaddress]::Parse($IPAddress).Address -band [uint64][BitConverter]::ToUInt32([System.Linq.Enumerable]::Reverse([BitConverter]::GetBytes([uint32](0xFFFFFFFFL -shl (32 - $PrefixLength) -band 0xFFFFFFFFL))), 0);
#         [pscustomobject]@{
#             Addr = $IPAddress;
#             Prfx = $PrefixLength;
#             NwAddr = [ipaddress]::new($bitNwAddr).IPAddressToString + '/' + $PrefixLength;
#         };
#     }
# }

# # extend route metric
# $targetIfs = Get-NetAdapter -IncludeHidden | Where-Object InterfaceDescription -Match $targetIfName;
# $targetAddresses = $targetIfs | Get-NetIPAddress -AddressFamily IPv4 | Get-NetworkAddress;
# $targetIfs | Set-NetIPInterface -InterfaceMetric 2;
# $targetIfs | Get-NetRoute -AddressFamily IPv4 | Select-Object -PipelineVariable rt | Where-Object { $targetAddresses | Where-Object { $_.NwAddr -eq (Get-NetworkAddress $rt.DestinationPrefix.Split('/')[0] $_.Prfx).NwAddr } } | Set-NetRoute -RouteMetric 6000;


$env:DNS_IP=Get-NetAdapter -InterfaceDescription $targetIfName | Get-DnsClientServerAddress -AddressFamily 'IPv4'| Select-Object -ExpandProperty ServerAddresses;

